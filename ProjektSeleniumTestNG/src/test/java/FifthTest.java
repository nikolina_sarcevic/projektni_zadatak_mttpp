import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class FifthTest {
    //-------------------Global Variables-----------------------------------
    //Declare a Webdriver variable
    public WebDriver driver;
    //Declare a test URL variable
    public String testURL = "http://www.google.com";
    //----------------------Test Setup-----------------------------------
    @BeforeMethod
    public void setupTest() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        //Create a new ChromeDriver
        driver = new ChromeDriver();
        //Go to www.swtestacademy.com
        driver.navigate().to(testURL);
    }
    @Test
    public void googleSearchTest() throws InterruptedException {
        driver.manage().window().maximize();
        Thread.sleep(5000);
        WebElement searchTextBox = driver.findElement(By.name("q"));
        searchTextBox.sendKeys("ikea");
        searchTextBox.submit();
        Thread.sleep(5000);

        WebElement firstResultLink = driver.findElement(By.xpath("//*[@id=\"tads\"]/div/div/div/div/div[1]/a/div[1]"));
        firstResultLink.click();
        WebElement map = driver.findElement(By.xpath("/html/body/div[5]/div/div/div/div[1]/div/div/button[6]"));
        map.click();
        Thread.sleep((5000));

        WebElement location = driver.findElement(By.xpath("//*[@id=\"tab-stores\"]/nav/ul/li[2]/a"));
        location.click();

        WebElement element = driver.findElement(By.xpath("//*[@id=\"16763c7f-912f-11eb-a434-bd1dcf8f1501\"]"));

        JavascriptExecutor js = (JavascriptExecutor) driver;

        js.executeScript("arguments[0].scrollIntoView(true);", element);

        Thread.sleep(2000);

        WebElement route = driver.findElement(By.xpath("//*[@id=\"16763c80-912f-11eb-a434-bd1dcf8f1501\"]/pub-hide-empty-link/div/a"));
        route.click();

        WebElement mylocation = driver.findElement(By.xpath("//*[@id=\"sb_ifc50\"]/input"));
        mylocation.sendKeys("osijek");
        mylocation.sendKeys(Keys.ENTER);
        Thread.sleep(5000);

        WebElement chooseRoute = driver.findElement(By.xpath("//*[@id=\"section-directions-trip-0\"]"));
        chooseRoute.click();



    }
    //---------------Test TearDown-----------------------------------
    @AfterMethod
    public void teardownTest() {
//Close browser and end the session
        driver.quit();
    }

}
