import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class ThirdTest {
    //-------------------Global Variables-----------------------------------
    //Declare a Webdriver variable
    public WebDriver driver;
    //Declare a test URL variable
    public String testURL = "http://www.google.com";
    //----------------------Test Setup-----------------------------------
    @BeforeMethod
    public void setupTest() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        //Create a new ChromeDriver
        driver = new ChromeDriver();
        //Go to www.swtestacademy.com
        driver.navigate().to(testURL);
    }
    @Test
    public void googleSearchTest() throws InterruptedException {
        driver.manage().window().maximize();
        Thread.sleep(5000);
        WebElement searchTextBox = driver.findElement(By.name("q"));
        searchTextBox.sendKeys("ikea");
        searchTextBox.submit();
        Thread.sleep(5000);

        WebElement firstResultLink = driver.findElement(By.xpath("//*[@id=\"tads\"]/div/div/div/div/div[1]/a/div[1]"));
        firstResultLink.click();
        WebElement product = driver.findElement(By.xpath("/html/body/div[5]/div/div/div/div[1]/div/div/button[1]"));
        product.click();
        Thread.sleep((5000));
        WebElement furniture = driver.findElement(By.xpath("//*[@id=\"hnf-carousel__tabs-navigation-products\"]/div[2]/a"));
        furniture.click();
        WebElement sofa = driver.findElement(By.xpath("//*[@id=\"hnf-dropdown-fu001\"]/div/div/div/ul[2]/li[2]/a"));
        sofa.click();

        WebElement element = driver.findElement(By.xpath("//*[@id=\"product-list\"]/div[1]/div[1]/div/div[2]/a/img[2]"));

        // Stvorite JavaScriptExecutor
        JavascriptExecutor js = (JavascriptExecutor) driver;

        // Skrolajte do određenog elementa
        js.executeScript("arguments[0].scrollIntoView(true);", element);

        Thread.sleep(2000);

        WebElement addToCart = driver.findElement(By.xpath("//*[@id=\"product-list\"]/div[1]/div[1]/div/div[4]/div/button[1]"));
        addToCart.click();



    }
    //---------------Test TearDown-----------------------------------
    @AfterMethod
    public void teardownTest() {
//Close browser and end the session
        driver.quit();
    }

}
