# Projektni_zadatak_MTTPP

# Nikolina Šarčević

Ovaj projekt je izrađen pomoću programskog jezika Java unutar IntelliJ IDEA programa. U projektu su testirane funkcionalnosti stranice Ikea pomoću 5 testnih slučajeva.

Unutar prvog testa se ispituje hoće li se unosom teksta ikea, kao prvi rezultat prikazati ciljana stranica (Ikea).

Drugi test se odnosi na provjeravanje hoće li se istim tim unosom prikazati ispravna poveznica stranice.

Trećim testom se ispituje funkcionalnost dodavanja proizvoda u košaricu tako da se uđe na željenu stranicu, odabere se gumb "Proizvodi", zatim gumb "Namještaj", zatim "Sofa" te se skrola do željenog elementa koji se zatim doda u košaricu.

Četvrti testni slučaj ispituje funkcionalnosti razvrstavanja proizvoda unutar drop down elementa gdje se odabire razvrstavanje "Najnovije".

Peti slučaj ispituje ispravnost prikazivanja rute od trenutne lokacije (Osijek) do željene lokacije (Ikea).

Također u projektu je napravljena i testng.xml datoteka koja pokreće prvi test. 